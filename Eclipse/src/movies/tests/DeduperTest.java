package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import movies.importer.*;

import org.junit.jupiter.api.Test;

class DeduperTest {

	@Test
	public void tester() {
		ArrayList <String> test=new ArrayList<String>();
		test.add("2016	the masked saint	111	kaggle");
		
		test.add("2016	the masked saint	111	imdb");
				ArrayList <String> testback=new ArrayList<String>();
				testback.add("2016	the masked saint	111	kaggle;imdb");
				Deduper e=new Deduper(" srcDir", " outputDir");
				assertEquals(testback,e.process(test));
	}
	
		
	
	
	
	
	@Test
	public void tester2() {
		ArrayList <String> test=new ArrayList<String>();
		
		
		test.add("2016	the masked saint	122	kaggle");
		test.add("2016	the masked saint	117	imdb");
		test.add("2016	the masked saint	112	kaggle");
		test.add("2016	the masked saint	107	kaggle");
				ArrayList <String> testback1=new ArrayList<String>();
				
				testback1.add("2016	the masked saint	107	kaggle;imdb");
				Deduper e=new Deduper(" srcDir", " outputDir");
				assertEquals(testback1,e.process(test));
	}
	
	public void tester3() {
		ArrayList <String> test=new ArrayList<String>();
		
		
		test.add("2016	the masked saint	107	kaggle");
		test.add("2016	the masked saint	112	imdb");
		test.add("2016	the masked saint	117	kaggle");
		test.add("2016	the masked saint	122	imdb");
				ArrayList <String> testback1=new ArrayList<String>();
				
				testback1.add("2016	the masked saint	107	kaggle;imdb");
				testback1.add("2016	the masked saint	117	kaggle;imdb");
				Deduper e=new Deduper(" srcDir", " outputDir");
				assertEquals(testback1,e.process(test));
	}
	
	public void tester4() {
		ArrayList <String> test=new ArrayList<String>();
		
		
		test.add("2016	the masked saint	107	kaggle");
		test.add("2016	the masked saint	117	imdb");
		test.add("2016	the masked saint	112	kaggle");
		test.add("2016	the masked saint	122	imdb");
				ArrayList <String> testback1=new ArrayList<String>();
				
				testback1.add("2016	the masked saint	107	kaggle");
				testback1.add("2016	the masked saint	117	kaggle;imdb");
				Deduper e=new Deduper(" srcDir", " outputDir");
				assertEquals(testback1,e.process(test));}
	
	

}

