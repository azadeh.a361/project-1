package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import movies.importer.Movie;

class MovieTest {

	@Test
	
	public void equaltester() {
	Movie o=new  Movie("2016","the masked saint","111","kaggle");
	Movie a=new Movie("2016","the masked saint","116","kaggle");
	 assertEquals(true,o.equals(a));}
	 
	 @Test
		
		public void equaltester1() {
		Movie o=new  Movie("2016","the masked saint","111","kaggle");
		Movie a=new Movie("2016","the masked saint","106","kaggle");
		 assertEquals(true,o.equals(a));
		
	}



	@Test
	
	public void equaltester2() {
	Movie o=new  Movie("2017","the masked saint","111","kaggle");
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	 assertEquals(false,o.equals(a));
	
}
	
@Test
	
	public void equaltester3() {
	Movie o=new  Movie("2016","the mask saint","111","kaggle");
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	 assertEquals(false,o.equals(a));
	
}
@Test
public void equaltester4() {
	Movie o=new  Movie("2016","the masked saint","111","kaggle");
	Movie a=new Movie("2016","the masked saint","111","imdb");
	 assertEquals(true,o.equals(a));}
	 
	 @Test
	 public void equaltester5() {
	 	Movie o=new  Movie("2016","the masked saint","111","imdb");
	 	Movie a=new Movie("2016","the masked saint","minute","imdb");
	 	 assertEquals(false,o.equals(a));
	
}
@Test
public void getNameTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	assertEquals("the masked saint",a.getName());
}
@Test
public void getReleaseYearTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	assertEquals("2016",a.getReleaseYear());
}
@Test
public void getRuntimeTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	assertEquals("111",a.getRuntime());}
@Test
public void getSourceTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	assertEquals("kaggle",a.getSource());}
@Test
public void setNameTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	a.setName("mask");
	assertEquals("mask",a.getName());}
@Test
public void setRunTimeTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	a.setRuntime("120");
	assertEquals("120",a.getRuntime());}

@Test
public void setSourceTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	a.setSource("imdb");
	assertEquals("imdb",a.getSource());}


@Test
public void toStringTest() {
	Movie a=new Movie("2016","the masked saint","111","kaggle");
	assertEquals("2016	the masked saint	111	kaggle",a.toString());}

}






