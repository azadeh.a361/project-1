package movies.importer;

import java.util.ArrayList;
/**
 * A class that extends Processor class
 * @author Azadeh Ahmadi
 *
 */

public class Normalizer extends Processor {
	
	public 	Normalizer( String srcDir, String outputDir) {
		super(srcDir,outputDir,false);}
	/**  method that gets the data from KaggleImporter.  
	 * it changes the Strings to lowercase and cuts the minute from runtime
	 * @param input An ArrayList<String> representing the input to the processor
	 * @return Returns an ArrayList<String> representing the results of applying this processor
	 */
		public  ArrayList<String> process(ArrayList<String> input){
			ArrayList<String> normalized = new ArrayList<String>();
			for(int i=0;i<input.size();i++) {
				String x=input.get(i);
				String []line=x.split("\\t");
				Movie xy=new Movie (line[0],line[1],line[2],line[3]);
				String title=xy.getName();
				String Ltitle=title.toLowerCase();
				String source=xy.getSource();
				String lsource=source.toLowerCase();
				xy.setName(Ltitle);
				xy.setSource(lsource);
				String runtime=xy.getRuntime();
				String cutruntime=runtime.substring(0,runtime.indexOf(" "));
				xy.setRuntime(cutruntime);
				String s=xy.toString();
				normalized.add(s);
			}
			
return normalized;
}
}

