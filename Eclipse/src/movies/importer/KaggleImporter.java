package movies.importer;

import java.util.ArrayList;
/**
 * A class that extends Processor class
 * @author Azadeh Ahmadi
 *
 */

public class KaggleImporter extends Processor{
	
public 	KaggleImporter( String srcDir, String outputDir) {
	super(srcDir,outputDir,true);}
/**  method that gets the raw data from Rotten Tomatoes and import them into a file
 * it omits some of the datas and keeps year title runtime and adds the source .
 * @param input An ArrayList<String> representing the input to the processor
 * @return Returns an ArrayList<String> representing the results of applying this processor
 */
	public  ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> imported = new ArrayList<String>();
		
		final int indexOfTitle=15;
		final int indexOfRuntime=13;
		final int rowLength=21;
		
		
		for (int i=0; i<input.size();i++) {
			String line=input.get(i);
				String[]coloumn=line.split("\\t");
				if (coloumn.length==rowLength) {
					Movie sourceMovie=new Movie(coloumn[rowLength-1],coloumn[indexOfTitle],coloumn[indexOfRuntime],"Kaggle") ;
					String x=sourceMovie.toString();
					imported.add(x);
					/*line1.indexOf("Title")*/
					
					
				}
				
	
}
		return imported;
	
}
}
