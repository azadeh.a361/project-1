package movies.importer;

import java.io.IOException;
/**
 * A class that includes the main method  that creates different Processor Objects
 *  and adds them to the Processor[]then uses processAll method to execute them.
 * @author Azadeh Ahmadi
 *
 */

public class ImportPipeline {

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
Processor[] alltask=new Processor[3];
alltask[0]=new KaggleImporter( "C:\\Users\\14389\\Documents\\java\\input" ,  "C:\\Users\\14389\\Documents\\java\\output");
alltask[1]=new Normalizer(  "C:\\Users\\14389\\Documents\\java\\output",  "C:\\Users\\14389\\Documents\\java\\finalOutput");
alltask[2]=new Deduper(  "C:\\Users\\14389\\Documents\\java\\finalOutput",  "C:\\Users\\14389\\Documents\\java\\finalfinalOutput");
		
	processAll(alltask) ;	
		
	}
	/**  method that goes through the Processor[]in aloop and executes them . 
	 * 
	 * @param input An Array of Processors
	 * 
	 */
	public static void processAll(Processor[]action) throws IOException {
		for (int i=0; i<action.length;i++) {
			
			action[i].execute() ;
	}

}}
