package movies.importer;
/**
 * A class that defines the Object Movie  and has necessary methods in  it
 * @author Azadeh Ahmadi
 *
 */
public class Movie {
	private String releaseYear;
	private String name;
	private String runtime;
	private String source;


public Movie(String releaseYear,String name,String runtime,String source) {
	this. releaseYear= releaseYear;
	this.name=name;
	this.runtime=runtime;
	this.source=source;
}

public String getReleaseYear() {
	return this. releaseYear;
}
public String getName() {
	return this. name;
}
public String getRuntime() {
	return this.runtime;
}
public String getSource() {
	return this.source;
}
public void setName(String name) {
	this.name=name;
}

public void setRuntime(String runtime) {
	this.runtime=runtime;
}
public void setSource(String source) {
	this.source=source;}
/**  A method that override the toString method for Object Movie 
 * the fields are separated y a tab
 * 
 * @return Returns String representing the Movie . 
 */
public String toString() {
	String s=this. releaseYear+"	"+this.name+"	"+this.runtime+"	"+this.source;
	
	return s;
}

/**  A method that override the equals method to include new rules .
 * the two  equal Movie object have same name and release year and their runtime is not more that 5 min apart.
 * @param input An object so that its the same as equals method which is overriden
 * @return Returns boolean 
 */
@Override

public boolean equals(Object o) {
	try {Integer.parseInt(this.runtime);
	Integer.parseInt(((Movie)o).runtime);
	}
	catch (final NumberFormatException e) {
		
		return false;}
	
	
	if (this.name.equals(((Movie)o).name) && this.releaseYear.equals(((Movie)o).releaseYear) && Integer.parseInt(this.runtime)-Integer.parseInt(((Movie)o).runtime)<=5  && Integer.parseInt(this.runtime)-Integer.parseInt(((Movie)o).runtime)>=-5)  
		
	return true;
	else
		return false;
	}

}



